﻿namespace CorvallisOR.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CorvallisOR.Models.ForumDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CorvallisOR.Models.ForumDb context)
        {
            // This method will be called after migrating to the latest version.
            // Ignore till then

            // You can use the DbSet<T>.AddOrUpdate() helper extension method 
            // to avoid creating duplicate seed data. E.g.
            //
            //   context.People.AddOrUpdate(
            //     p => p.FullName,
            //     new Person { FullName = "Sean White" },
            //     new Person { FullName = "Merrick Simms" },
            //     new Person { FullName = "Brian Bird" }
            //   );
            //
        }
    }
}
