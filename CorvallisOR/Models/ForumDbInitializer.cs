﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorvallisOR.Models
{
    public class ForumDbInitializer : System.Data.Entity.DropCreateDatabaseAlways<ForumDb>
    {
        
        protected UserManager<Member> userManager = new UserManager<Member>(
                new UserStore<Member>(new ForumDb()));

        protected override void Seed(ForumDb context)
        {
            /*
            context.Members.Add(new Member { Name="Sean White", Email="sw@asptest.com", UserName="user1" });
            context.Members.Add(new Member { Name="Brian Bird", Email="bb@asptest.com", UserName="user2" });
            context.Topics.Add(new Topic { Subject = "Pool Party" });
            context.Topics.Add(new Topic { Subject = "An Event Test" });
            context.Topics.Add(new Topic { Subject = "Frabjous" });
            context.Messages.Add(new Message
                {
                    Topic = context.Topics.Local[0],
                    Date = DateTime.Parse("1/1/17"),
                    Member = context.Members.Local[0],
                    Body = "Pool Party"
                });
            context.Messages.Add(new Message
                {
                    Topic = context.Topics.Local[1],
                    Date = DateTime.Parse("1/22/17"),
                    Member = context.Members.Local[1],
                    Body = "I'm an event demo!"
                });
            context.Messages.Add(new Message
            {
                Topic = context.Topics.Local[2],
                Date = DateTime.Parse("7/7/77777"),
                Member = context.Members.Local[1],
                Body = "Tell them the event is frabjous. They respect words they don't understand."
            });
            context.Messages.Add(new Message
            {
                Topic = context.Topics.Local[1],
                Date = DateTime.Parse("1/25/17"),
                Member = context.Members.Local[0],
                Body = "Another Event Test"
            });
             */
            base.Seed(context);
        }
    }
}