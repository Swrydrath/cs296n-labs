﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorvallisOR.Models
{
    public class UserRoleVM
    {
        public string UserID { get; set; }
        public string Name { get; set; }
        public IEnumerable<SelectListItem> UserList { get; set; }
        public string Add { get; set; }
        public string Delete { get; set; }
        public IEnumerable<SelectListItem> RoleList { get; set; }
        public IEnumerable<SelectListItem> Available { get; set; }
    }
}