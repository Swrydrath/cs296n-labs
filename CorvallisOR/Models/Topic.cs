﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorvallisOR.Models
{
    public class Topic
    {
        public int TopicID { get; set; }
        [Required(ErrorMessage = "You must enter a topic")]
        public string Subject { get; set; }

        public List<Message> Messages { get; set; }
    }
}