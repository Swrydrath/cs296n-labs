﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorvallisOR.Models
{
    public class Message
    {
        public int MessageID { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public int TopicID { get; set; }
        public virtual Topic Topic { get; set; }
        [Display(Name = "Message")]
        [Required(ErrorMessage = "A message body is required.")]
        public string Body { get; set; }
        public virtual Member Member { get; set; }

        public virtual List<Topic> Topics { get; set; }
        public virtual List<Member> Members { get; set; }
    }
}