﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CorvallisOR.Models;

namespace CorvallisOR.Controllers
{
    public class ForumController : Controller
    {
        private ForumDb db = new ForumDb();

        // GET: Forum
        public ActionResult Index()
        {
            return View(db.Messages.ToList());
        }

        // GET: Forum/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // GET: Forum/Create
        [Authorize(Roles = "Standard")]
        public ActionResult Create()
        {
            CreateMessageVM createVM = new CreateMessageVM
            {
                Topics = new SelectList(db.Topics.OrderBy(t => t.Subject), "TopicID", "Subject"),
                Message = new Message()
            };
            return View(createVM);
        }

        // POST: Forum/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Standard")]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TopicID,Topics,Message,Subject")]CreateMessageVM createVM)
        {
            if (ModelState.IsValid)
            {
                Message message = createVM.Message;
                message.Topic = db.Topics.Find(createVM.TopicID);
                if (message.Topic == null || message.Topic.Subject != createVM.Subject)
                {
                    Topic topic = new Topic { Subject = createVM.Subject };
                    db.Topics.Add(topic);
                    db.SaveChanges();
                    message.Topic = topic;
                }
                string currentUserId = User.Identity.GetUserId();
                message.Member = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                message.Date = DateTime.Now;
                db.Messages.Add(message);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                createVM.Topics = new SelectList(db.Topics.OrderBy(t => t.Subject), "TopicID", "Subject");
                createVM.Message = new Message();
            }

            return View(createVM);
        }

        // GET: Forum/Edit/5
        [Authorize(Roles = "Standard")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            if (message.Member.Id != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.Forbidden);
            }
            CreateMessageVM createVM = new CreateMessageVM
            {
                ID = message.MessageID,
                Topics = new SelectList(db.Topics.OrderBy(t => t.Subject), "TopicID", "Subject"),
                Message = message
            };
            createVM.Subject = message.Topic.Subject;
            return View(createVM);
        }

        // POST: Forum/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Standard")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TopicID,Topics,Message,Subject")]CreateMessageVM createVM)
        {
            bool isDirty = false;
            bool updateTopic = false;
            Topic topic = new Topic();
            if (ModelState.IsValid)
            {
                Message message = db.Messages.Find(createVM.ID);
                Topic oldTopic = db.Topics.Find(message.Topic.TopicID);
                if (oldTopic.Subject != createVM.Subject)
                {
                    isDirty = true;
                    if (db.Messages.Where(m => m.Topic.Subject == oldTopic.Subject).Count() == 1) updateTopic = true;
                    topic = db.Topics.Find(createVM.TopicID);
                    if (topic == null)
                    {
                        topic = db.Topics.SingleOrDefault(t => t.Subject == createVM.Subject);
                        if (topic == null)
                        {
                            topic = new Topic { Subject = createVM.Subject };
                        }
                    }
                }
                if (message.Body != createVM.Message.Body)
                {
                    isDirty = true;
                    message.Body = createVM.Message.Body;
                }
                if (isDirty)
                {
                    if (updateTopic)
                    {
                        oldTopic.Subject = topic.Subject;
                        db.Entry(oldTopic).State = EntityState.Modified;
                        topic = oldTopic;
                    }
                    message.Topic = topic;
                    db.Entry(message).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View(createVM);
        }

        // GET: Forum/Delete/5
        [Authorize(Roles = "Standard")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            if (message.Member.Id != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.Forbidden);
            }
            return View(message);
        }

        // POST: Forum/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Standard")]
        public ActionResult DeleteConfirmed(int id)
        {
            Message message = db.Messages.Find(id);
            Topic topic = db.Topics.Find(message.Topic.TopicID);
            int topicCount = db.Messages.Where(m => m.Topic.TopicID == topic.TopicID).Count();
            db.Messages.Remove(message);
            if (topicCount == 1)
            {
                db.Topics.Remove(topic);
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Search(string searchString)
        {
            var messages = db.Messages.Where((System.Linq.Expressions.Expression<Func<Message, bool>>)(m => m.Topic.Subject.Contains(searchString)));
            ViewBag.SearchString = searchString;
            return View(messages);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
