﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CorvallisOR.Models;

namespace CorvallisOR.Controllers
{
    public class NewsController : Controller
    {
        News[] story = new News[]
        {
            new News { Date=DateTime.Now, Title="Mid-Valley Local Emergency Planning Committee Annual Meeting", Story="The Mid-Valley Local Emergency Planning Committee, which assists public safety officials with emergency preparedness in Linn and Benton, will host its annual meeting from 7 to 8:30 p.m. Feb. 10. The meeting will include an overview of the FEMA's Community Emergency Response Team (CERT) program,..." },
            new News { Date=DateTime.Now, Title="Parents' Night Out", Story="Parents' Night Out Drop off the kids while you go out for a night on the town. Kids ages 3 to 6 will play games in the activity room and those 7 to 12 can also go for a swim! We'll provide fun couselors, games, activities and a healthy snack. Preregistration preferred. Fee: $15 single..." },
            new News { Date=DateTime.Now, Title="Family Movie Swim", Story="Family Movie Swim A family-friendly movie will be projected pool side each month. Call for movie details 541-766-7946. Regular Admission Rates Apply  " },
            new News { Date=DateTime.Parse("03-01-2016"), Story="Kayak Roll Session", Title="Kayak Roll Session: Bring in your kayak and paddle around the indoor 50 meter pool. Arrive by 8:30 pm for payment/admission." }
        };

        // GET: News
        public ActionResult Index()
        {
            News[] today = story.Where(e => e.Date == DateTime.Now).ToArray();
            return View(today);
        }

        public ActionResult Archive()
        {
            News[] past = story.Where(e => e.Date != DateTime.Now).ToArray();
            return View(past);
        }
    }
}