﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CorvallisOR.Models;

namespace CorvallisOR.Controllers
{
    [Authorize(Roles = "Admin, SuperAdmin")]
    public class RolesController : Controller
    {
        private ForumDb context = new ForumDb();
        // GET: Roles
        public ActionResult Index()
        {
            var roles = context.Roles.ToList();
            return View(roles);
        }

        // GET: /Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Roles/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole()
                {
                    Name = collection["RoleName"]
                });
                context.SaveChanges();
                ViewBag.ResultMessage = "Role created successfully !";
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(string RoleName)
        {
            var thisRole = context.Roles.Where(r => r.Name.Equals(RoleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            context.Roles.Remove(thisRole);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /Roles/Edit/5
        public ActionResult Edit(string roleName)
        {
            var thisRole = context.Roles.Where(r => r.Name.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            return View(thisRole);
        }

        //
        // POST: /Roles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Microsoft.AspNet.Identity.EntityFramework.IdentityRole role)
        {
            try
            {
                context.Entry(role).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ManageUserRoles()
        {
            // prepopulate roles for the view dropdown
            UserRoleVM userRole = new UserRoleVM
            {
                UserList = context.Users.OrderBy(u => u.Name).ToList().Select(u =>
                  new SelectListItem { Value = u.Id, Text = u.Name }).ToList(),
                RoleList = context.Roles.OrderBy(r => r.Name).ToList().Select(rr =>
                  new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList()
            };
            Session["userID"] = null;
            return View(userRole);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RoleAddToUser([Bind(Include = "Add")]UserRoleVM userRole)
        {
            if (Session["userID"] != null)
            {
                string userID = (string)Session["userID"];
                if (userRole.Add != null)
                {
                    var manager = new UserManager<Member>(
                    new UserStore<Member>(
                        new ForumDb()));
                    manager.AddToRole(userID, userRole.Add);
                }
                userRole = PopulateVM(userID);
                return View("ManageUserRoles", userRole);
            }
            return View("ManageUserRoles");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetRoles([Bind(Include = "UserID")]UserRoleVM userRole)
        {
            if (userRole.UserID != null)
            {
                Session["userID"] = userRole.UserID;
                userRole = PopulateVM(userRole.UserID);
                return View("ManageUserRoles", userRole);
            }
            else return RedirectToAction("ManageUserRoles");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteRoleForUser([Bind(Include = "Delete")]UserRoleVM userRole)
        {
            if (Session["userID"] != null)
            {
                string userID = (string)Session["userID"];
                if (userRole.Delete != null)
                {
                    var manager = new UserManager<Member>(
                        new UserStore<Member>(new ForumDb()));
                    if (manager.IsInRole(userID, userRole.Delete))
                    {
                        manager.RemoveFromRole(userID, userRole.Delete);
                    }
                }
                userRole = PopulateVM(userID);
                return View("ManageUserRoles", userRole);
            }
            return View("ManageUserRoles");
        }


        protected UserRoleVM PopulateVM(string userID)
        {
            Member user = context.Users.SingleOrDefault(u => u.Id == userID);
            var manager = new UserManager<Member>(
            new UserStore<Member>(
                new ForumDb()));
            IEnumerable<string> roles = manager.GetRoles(user.Id);
            ViewBag.RolesForThisUser = manager.GetRoles(user.Id);
            UserRoleVM userRole = new UserRoleVM
            {
                UserList = context.Users.OrderBy(u => u.Name).ToList().Select(u =>
                    new SelectListItem { Value = u.Id, Text = u.Name, Selected = (u.Id == user.Id) }).ToList(),
                RoleList = context.Roles.Where(r => !roles.Contains(r.Name)).OrderBy(r => r.Name).ToList().Select(rr =>
                    new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList(),
                Name = user.Name,
                Available = manager.GetRoles(user.Id).Select(x => new SelectListItem() { Text = x.ToString() })
            };
            return userRole;
        }
    }
}